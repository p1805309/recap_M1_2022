# recap_M1_2022

## M1IF01 : Gestion de projet et génie logiciel

[M1IF01 Folder](M1IF01)

## M1IF02 : Informatique graphique et image

[M1IF02 Folder](M1IF02)

## M1IF03 : Conception d'applications web

[M1IF03 Folder](M1IF03)

## M1IF04 : Apprentissage et analyse de données

[M1IF04 Folder](M1IF04)

## M1IF05 : Réseaux

[M1IF05 Folder](M1IF05)

## M1IF06 : Bases de l'intelligence artificielle

[M1IF06 Folder](M1IF06)

## M1IF07 : Algorithmes pour l'optimisation

[M1IF07 Folder](M1IF07)

## M1IF11 : Ouverture à la recherche

[M1IF11 Folder](M1IF11)

## M1IF18 : Systèmes avancés

[M1IF18 Folder](M1IF18)

## M1IF23 : Programmation avancée

[M1IF23 Folder](M1IF23)

## M1IF24 : Gestion de projet et génie logiciel

[M1IF24 Folder](M1IF24)

## M1IF25 : Programmation avancée

[M1IF25 Folder](M1IF25)

## M1IF31 : Insertion professionnelle

[M1IF31 Folder](M1IF31)

Note: Files in this folder are manually added